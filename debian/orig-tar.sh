#!/bin/sh

VERSION=$2
TAR=../guava-libraries-18_$VERSION.orig.tar.xz

# delete the source jar downloaded by uscan
rm $3

# download and extract the real tarball
wget http://guava-libraries.googlecode.com/archive/v$VERSION.zip -O guava-libraries-$VERSION.zip
unzip guava-libraries-$VERSION.zip
rm guava-libraries-$VERSION.zip

# rebuild the tarball
XZ_OPT='--best -v' tar -cJ --exclude '*.jar' -f $TAR guava-libraries-*

rm -rf guava-libraries-*
